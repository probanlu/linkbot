const { cmdPrefix } = require('../bot');
const Discord = require('discord.js');
const validUrl = require('valid-url');
const path = require('path');
const bandcampFilePath = path.join(__dirname + '/../storage/bandcamp.json');
const botstorage = require(bandcampFilePath); // path may vary
const fs = require('fs');

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);

    let bcLink = args[1];
    //args.slice(1).join(" ");

    function findWord(word, str) {
        return RegExp('\\b'+ word +'\\b').test(str)
    }

    if (!bcLink) {
        const improperUsage = new Discord.MessageEmbed()
            .setTitle("How to use Set Bandcamp")
            .setColor("#ed411f")
            .setDescription("Use this command to set your Bandcamp link\nthrough the bot. It will appear on your profile\nand will be accesible to all users via the $bc command.")
            .addField("Example Usage:", "$setbc https://c418.bandcamp.com/")
            .setTimestamp();

        return msg.channel.send(improperUsage);
    }

    if (bcLink.toLowerCase() === "--clear") {
        msg.channel.send("Got it, your Bandcamp has been cleared.");
        botstorage[msg.author.id] = "";
        return fs.writeFileSync(directory, JSON.stringify(botstorage));
    }

    if (!validUrl.isUri(bcLink)) {
        return msg.channel.send("Provide a valid site on the internet!");
    }

    if (bcLink.startsWith('http:')) {
        bcLink = 'https' + bcLink.slice('http'.length);
    }

    if (!/^https:\/\/[a-z0-9-]+\.bandcamp\.com\/?$/.test(bcLink)) {
        return msg.channel.send("Provide a valid Bandcamp link plz");
    }

    if (bcLink.length >= 100) {
        return msg.channel.send("Your Bandcamp link has to be under **100** characters!");
    }

    botstorage[msg.author.id] = bcLink;
    fs.writeFileSync(bandcampFilePath, JSON.stringify(botstorage));

    const bandcampEmoji = client.emojis.cache.find(emoji => emoji.name === "bandcamp");
    const successEmbed = new Discord.MessageEmbed()
        .setTitle("Bandcamp Successfully Set!")
        .setColor("#629aa9")
        .setDescription(bandcampEmoji.toString() + " Your Bandcamp has been successfully set!")
        .setFooter("View your link on your profile with $profile")
        .setTimestamp();

    msg.channel.send(successEmbed);
}
