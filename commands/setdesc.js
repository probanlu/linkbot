const { cmdPrefix } = require('../bot');
const Discord = require('discord.js');
const path = require('path');
const descriptionsFilePath = path.join(__dirname + '/../storage/descriptions.json');
const botstorage = require(descriptionsFilePath); // path may vary
const fs = require('fs');

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);
    let desc = args.slice(1).join(" ");

    if (!desc) {
        const improperUsage = new Discord.MessageEmbed()
            .setTitle("How to use Set Description")
            .setColor("#ed411f")
            .setDescription("Use this command to set your description\nthrough the bot. It will appear on your profile.\nYou can use this command to let others know a little\nbit about you.")
            .addField("Example Usage:", "$setd hi there!")
            .setTimestamp();

        return msg.channel.send(improperUsage);
    }

    if (desc.toLowerCase() === "--clear") {
        msg.channel.send("Got it, your description has been cleared.");
        botstorage[msg.author.id] = "";
        return fs.writeFileSync(descriptionsFilePath, JSON.stringify(botstorage));
    }

    if(desc.length > 280) {
        return msg.channel.send("Your description is too long! Make sure it is under **280** characters long. Think tweet long.");
    }

    botstorage[msg.author.id] = desc;
    fs.writeFileSync(descriptionsFilePath, JSON.stringify(botstorage));

    const checkEmoji = client.emojis.cache.find(emoji => emoji.name === "tommycheck");
    const successEmbed = new Discord.MessageEmbed()
        .setTitle("Description Successfully Set!")
        .setColor("#a1ff5e")
        .setDescription(checkEmoji.toString() + " Description successfully changed!")
        .setFooter("View your profile with $profile")
        .setTimestamp();

    msg.channel.send(successEmbed);
};
