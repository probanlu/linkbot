const { cmdPrefix } = require('../bot');
const Discord = require('discord.js');
const path = require('path');
const favoritesFilePath = path.join(__dirname + '/../storage/favorite.json');
const botstorage = require(favoritesFilePath); // path may vary
const fs = require('fs');

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);
    let favorite = args.slice(1).join(" ");

    botstorage[msg.author.id] = favorite;
    fs.writeFileSync(favoritesFilePath, JSON.stringify(botstorage));

    if (favorite.toLowerCase() === "--clear") {
        msg.channel.send("Got it, your favorite artist has been cleared.");
        botstorage[msg.author.id] = "";
        return fs.writeFileSync(favoritesFilePath, JSON.stringify(botstorage));
    }

    if (favorite.length > 30) {
        return msg.channel.send("Your favorite artist's name is too long! Make sure it is under **30** characters long! If this is the actual artist's name, email a mod or the creator and we'll make an exception for you.");
    }

    const checkEmoji = client.emojis.cache.find(emoji => emoji.name === "tommycheck");
    const successEmbed = new Discord.MessageEmbed()
        .setTitle("Successfully set your favorite musician " + checkEmoji.toString())
        .setColor("#27f568")
        .setDescription("On your profile your favorite musician will now be: `" +favorite+ "`! Why would you put this information in? Everyone has some form of inspiration! It's important to let others interested in your work know who's behind your madness. Or maybe not.")
        .setTimestamp();

    msg.channel.send(successEmbed);
};
