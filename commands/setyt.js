const { cmdPrefix } = require('../bot');
const Discord = require('discord.js');
const path = require('path');
const youtubeFilePath = path.join(__dirname + '/../storage/youtube.json');
const botstorage = require(youtubeFilePath); // path may vary
const fs = require('fs');

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);
    let link = args[1];

    if (link.toLowerCase() === "--clear") {
        msg.channel.send("Got it, your YouTube has been cleared.");
        botstorage[msg.author.id] = "";
        return fs.writeFileSync(youtubeFilePath, JSON.stringify(botstorage));
    }

    if (link.includes("watch?v=")) {
        msg.channel.send("Invalid YouTube channel link!");
        return;
    }

    if (!link.startsWith("https://www.youtube.com/c") &&  !link.startsWith("https://www.youtube.com/user/")) {
        msg.channel.send("Invalid YouTube channel link!");
        return;
    }

    if (link.length >= 100) {
        return msg.channel.send("Your Youtube link has to be under **100** characters!");
    }

    botstorage[msg.author.id] = args[1];
    fs.writeFileSync(youtubeFilePath, JSON.stringify(botstorage));

    const ytEmoji = client.emojis.cache.find(emoji => emoji.name === "yt");
    const successEmbed = new Discord.MessageEmbed()
        .setTitle("Youtube Successfully Set!")
        .setColor("#c4302b")
        .setDescription(ytEmoji.toString() + " Your Youtube has been successfully set!")
        .setFooter("View your link on your profile with $profile")
        .setTimestamp();

    msg.channel.send(successEmbed);
};
