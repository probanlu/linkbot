const { cmdPrefix } = require('../bot');
const Discord = require('discord.js');
const path = require('path');
const soundcloudFilePath = path.join(__dirname + '/../storage/soundcloud.json');
const botstorage = require(soundcloudFilePath); // path may vary
const fs = require('fs');

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);
    var link = args[1];
    //args.slice(1).join(" ");

    function findWord(word, str) {
        return RegExp('\\b'+ word +'\\b').test(str)
    }

    if (!link) {
        const improperUsage = new Discord.MessageEmbed()
            .setTitle("How to use Set Soundcloud")
            .setColor("#ed411f")
            .setDescription("Use this command to set your Soundcloud\nthrough the bot. It will appear on your profile.")
            .addField("Example Usage:", "$setsc https://soundcloud.com/c418")
            .setTimestamp();

        return msg.channel.send(improperUsage);
    }

    if (link.toLowerCase() === "--clear") {
        msg.channel.send("Got it, your Soundcloud has been cleared.");
        botstorage[msg.author.id] = "";
        return fs.writeFileSync(soundcloudFilePath, JSON.stringify(botstorage));
    }

    if (!link.startsWith("https://soundcloud.com/")) {
        return msg.channel.send("Provide a valid link plz");
    }

    if (link.length >= 100) {
        return msg.channel.send("Your Soundcloud link has to be under **100** characters!");
    }

    botstorage[msg.author.id] = link;
    fs.writeFileSync(soundcloudFilePath, JSON.stringify(botstorage));

    const soundcloudEmoji = client.emojis.cache.find(emoji => emoji.name === "soundcloud");
    const successEmbed = new Discord.MessageEmbed()
        .setTitle("Soundcloud Successfully Set!")
        .setColor("#ff7700")
        .setDescription(soundcloudEmoji.toString() + " Your Soundcloud has been successfully set!")
        .setFooter("View your link on your profile with $profile")
        .setTimestamp();

    msg.channel.send(successEmbed);
};
