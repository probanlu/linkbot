const { cmdPrefix } = require('../bot');
const Discord = require('discord.js');
const path = require('path');
const soundcloudFilePath = path.join(__dirname + '/../storage/soundcloud.json');
const botstorage = require(soundcloudFilePath); // path may vary

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);

    var userMention = msg.mentions.users.first();
    var id = args[1];
    var member;

    if (!userMention) {
        member = msg.guild.members.cache.get(id);
    }

    if (userMention) {
        member = msg.guild.members.cache.get(userMention.id)
    }

    if (!userMention && !args[1]) {
        member = msg.guild.members.cache.get(msg.author.id);
    }

    if (!member) {
        return msg.channel.send("Please mention a user or paste an ID!");
    }

    if (!botstorage[member.id] || botstorage[member.id].length <= 0) {
        return msg.channel.send(member.user.username + " doesn't have a Soundcloud!");
    }

    const soundcloudEmoji = client.emojis.cache.find(emoji => emoji.name === "soundcloud");
    const scEmbed = new Discord.MessageEmbed()
        .setAuthor(member.user.tag + "'s Soundcloud", member.user.displayAvatarURL({dynamic: true}))
        .setDescription(soundcloudEmoji.toString() +" "+ `Click [here](${botstorage[member.id]}) to view ${member.user.username}'s Soundcloud`)
        .setTimestamp();

    msg.channel.send(scEmbed);

    //msg.delete();

}