const { cmdPrefix } = require('../bot');
const Discord = require('discord.js');
const path = require('path');
const youtubeFilePath = path.join(__dirname + '/../storage/youtube.json');
const botstorage = require(youtubeFilePath); // path may vary

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);

    var userMention = msg.mentions.users.first();
    var id = args[1];
    var member;

    if (!userMention) {
        member = msg.guild.members.cache.get(id);
    }

    if (userMention) {
        member = msg.guild.members.cache.get(userMention.id)
    }

    if (!userMention && !args[1]) {
        member = msg.guild.members.cache.get(msg.author.id);
    }

    if (!member) {
        return msg.channel.send("Please mention a user or paste an ID!");
    }

    if (!botstorage[member.id] || botstorage[member.id].length <= 0) {
        return msg.channel.send(member.user.username + " doesn't have a Youtube setup yet!");
    }

    const bcEmbed = new Discord.MessageEmbed()
        .setColor("#c4302b")
        .setAuthor(member.user.tag + "'s Youtube", member.user.displayAvatarURL({dynamic: true}))
        .setDescription("Click [here](" +botstorage[member.id]+ ") to visit " +member.user.username+ "'s Youtube")
        .setTimestamp();

    msg.channel.send(bcEmbed);

    //msg.delete();
};
