const { cmdPrefix } = require('../bot');
const Discord = require('discord.js');
const path = require('path');
const bandcampFilePath = path.join(__dirname + '/../storage/bandcamp.json');
const botstorage = require(bandcampFilePath); // path may vary

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);

    var userMention = msg.mentions.users.first();
    var id = args[1];
    var member;

    let priz = client.emojis.cache.find(emoji => emoji.name === "priz");


    if (!userMention) {
        member = msg.guild.members.cache.get(id);
    }

    if (userMention) {
        member = msg.guild.members.cache.get(userMention.id)
    }

    if (!userMention && !args[1]) {
        member = msg.guild.members.cache.get(msg.author.id);
    }

    if (!member) {
        return msg.channel.send("Please mention a user or paste an ID!");
    }

    if (!botstorage[member.id] || botstorage[member.id].length <= 0) {
        if (member.user.username === "PRIZ ;]") {
            return msg.channel.send(priz.toString() + " " + member.user.username + " doesn't have a bandcamp setup yet!\n*He actually does:* https://przm.bandcamp.com/ ;]");
        }
        return msg.channel.send(member.user.username + " doesn't have a bandcamp setup yet!");
    }

    const bcEmbed = new Discord.MessageEmbed()
        .setColor("#629aa9")
        .setAuthor(member.user.tag + "'s Bandcamp", member.user.displayAvatarURL({dynamic: true}))
        .setDescription("Click [here](" +botstorage[member.id]+ ") to visit " + member.user.username + "'s Bandcamp")
        .setTimestamp();

    msg.channel.send(bcEmbed);

    //msg.delete();
};
