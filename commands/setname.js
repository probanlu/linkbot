const { cmdPrefix } = require('../bot');
const path = require('path');
const namesFilePath = path.join(__dirname + '/../storage/names.json');
const botstorage = require(namesFilePath); // path may vary
const fs = require('fs');

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);
    let name = args.slice(1).join(" ");

    if (name.toLowerCase() === "--clear") {
        msg.channel.send("Got it, your artist/moniker has been cleared.");
        botstorage[msg.author.id] = "";
        return fs.writeFileSync(namesFilePath, JSON.stringify(botstorage));
    }

    if (name.length > 30) {
        return msg.channel.send("Your name is too long! Make sure it is under **30** characters long!");
    }

    botstorage[msg.author.id] = name;
    fs.writeFileSync(namesFilePath, JSON.stringify(botstorage));

    const checkEmoji = client.emojis.cache.find(emoji => emoji.name === "tommycheck");
    msg.channel.send(checkEmoji.toString() + " Got it! Your musical name is now `" +botstorage[msg.author.id] + "`");
};
