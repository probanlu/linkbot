const { cmdPrefix } = require('../bot');
const Discord = require('discord.js');
const path = require('path');
const reportChannelsFilePath = path.join(__dirname + '/../storage/reportchannel.json');
const botstorage = require(reportChannelsFilePath);
const fs = require('fs');

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);
    var channelID = args[1];

    if (!msg.member.hasPermission("ADMINISTRATOR") || !msg.member.hasPermission("MANAGE_CHANNELS")) {
        return msg.channel.send("✋ Stop right there. I can't let you do that.");
    }

    if(String(channelID).startsWith("<#"))
        channelID = channelID.slice(2,-1); // <#123> --> 123

    if (!channelID) {
        return msg.channel.send("Needa specify a channel ID that I'll send reports to.");
    }

    var channel = msg.guild.channels.cache.get(channelID);

    if (!channel) {
        return msg.channel.send("Supply a valid channel ID, buddy ol' pal. If it is a valid channel, make sure I have permissions to see it.");
    }

    botstorage[msg.guild.id] = channelID;
    fs.writeFileSync(reportChannelsFilePath, JSON.stringify(botstorage));

    const successEmbed = new Discord.MessageEmbed()
        .setTitle("Report Channel Successfully Set!")
        .setColor("#8dff6e")
        .setDescription("📣 The report channel has been successfully set to `#" + channel.name + "`!")
        .setFooter("All user reports will go here!")
        .setTimestamp();

    msg.channel.send(successEmbed);
};
