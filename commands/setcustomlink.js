const { cmdPrefix } = require('../bot');
const Discord = require('discord.js');
const path = require('path');
const customLinksFilePath = path.join(__dirname + '/../storage/custom.json');
const botstorage = require(customLinksFilePath); // path may vary
const fs = require('fs');
const validUrl = require('valid-url');

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);
    let linkPlatform = args[2];
    let link = args[1];

    if (!linkPlatform) {
        platform = "Custom Link";
    }

    if (!link) {
        const improperUsage = new Discord.MessageEmbed()
            .setTitle("How to use Set Custom Link")
            .setColor("#ed411f")
            .setDescription("Use this command to set your custom link\nthrough the bot. It will appear on your profile.\nYou can use this command to promote your website or Twitter.")
            .addField("Example Usage:", "$setcustom https://github.com/user GitHub")
            .setTimestamp();

        return msg.channel.send(improperUsage);
    }

    if (link.toLowerCase() === "--clear") {
        msg.channel.send("Got it, your Bandcamp has been cleared.");
        botstorage[msg.author.id] = "";
        return fs.writeFileSync(customLinksFilePath, JSON.stringify(botstorage));
    }

    if (!validUrl.isUri(link)) {
        return msg.channel.send("Provide a valid site on the internet!");
    }

    if (link.length >= 100) {
        return msg.channel.send("Your custom link has to be under **100** characters!");
    }

    if (linkPlatform.length > 30) {
        return msg.channel.send("Your custom link platform has to be under **30** characters!");
    }

    if (linkPlatform.trim().length === 0) {
        return msg.channel.send("Specify a link platform");
    }

    botstorage[msg.author.id] = { platform: linkPlatform, link: link};
    fs.writeFileSync(customLinksFilePath, JSON.stringify(botstorage));

    const checkEmoji = client.emojis.cache.find(emoji => emoji.name === "tommycheck");
    const successEmbed = new Discord.MessageEmbed()
        .setTitle("Custom Link Successfully Set!")
        .setDescription(checkEmoji.toString() + " Custom link successfully set!")
        .setFooter("View your profile with $profile to see the link")
        .setTimestamp();

    msg.channel.send(successEmbed);
}
