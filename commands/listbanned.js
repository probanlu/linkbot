const Discord = require('discord.js');
const blacklist = require('./blacklistuser');

module.exports = async (client, msg) => {
    const { channel, author: authorUser, member: authorMember } = msg;

    if (!authorMember.hasPermission('ADMINISTRATOR')) {
        await channel.send('You need the `ADMINISTRATOR` permission to use this command.');
        return;
    }

    const sentEmbed = new Discord.MessageEmbed()
        .setTitle('Check your DMs!')
        .setDescription('A list of the blacklisted users in this server have been sent to you via DMs.')
        .setTimestamp();

    const blacklistedUserIds = await blacklist.getBlacklistedUserIds();
    if (blacklistedUserIds.length === 0) {
        await authorUser.send('Nobody is blacklisted!');
        return;
    }
    const blacklistEmbed = new Discord.MessageEmbed()
        .setTitle('Blacklisted Users')
        .setColor('#eb2a23')
        .setAuthor('Requested by ' + authorUser.username, authorUser.displayAvatarURL({dynamic: true}))
        .setDescription('This is a list of the blacklisted users!\nThey are not able to use to bot in any form.');
    const blacklistedUserPromises = blacklistedUserIds
        .map(id => client.users.fetch(id));

    for (let i = 0; i < blacklistedUserPromises.length; i++) {
        let user;
        try {
            user = await blacklistedUserPromises[i];
        } catch {
        }

        const id = blacklistedUserIds[i];
        if (user) {
            blacklistEmbed.addField(user.username, `has been blacklisted (user ID ${id})`);
        } else {
            blacklistEmbed.addField(id, 'has been blacklisted (user ID shown)');
        }
    }

    authorUser.send(blacklistEmbed);
    channel.send(sentEmbed);
};
