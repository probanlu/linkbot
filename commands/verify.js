const { cmdPrefix } = require('../bot');
const Discord = require('discord.js');
const path = require('path');
const userInfoFilePath = path.join(__dirname + '/../storage/userinfo.json');
const botstorage = require(userInfoFilePath); // path may vary
const fs = require('fs');

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);

    const userMention = msg.mentions.users.first();
    const id = args[1];
    let member;

    const reason = args.slice(2).join(' ') || 'No reason specified';

    if (!msg.member.hasPermission('ADMINISTRATOR')) {
        return msg.channel.send('You don\'t have permission to verify and unverify users!');
    }

    if (!userMention) {
        member = msg.guild.members.cache.get(id);
    }

    if (userMention) {
        member = msg.guild.members.cache.get(userMention.id);
    }

    if (!userMention && !args[1]) {
        member = msg.guild.members.cache.get(msg.author.id);
    }

    if (!member) {
        return msg.channel.send('Please mention a user or paste an ID!');
    }

    if (member.bot) {
        return msg.channel.send('You can\'t verify a bot!');
    }

    if (msg.author.id === member.id) {
        return msg.channel.send('You can\'t verify yourself!');
    }

    const verifiedEmoji = client.emojis.cache.find(emoji => emoji.name === 'verifiedartist');
    const congratsEmbed = new Discord.MessageEmbed()
        .setTitle(verifiedEmoji.toString() + " You've Been Verified!")
        .setColor("#1DA1F2")
        .setDescription("The administrators of this server have decided\nto add you to our verification program. Welcome aboard.")
        .addField(msg.author.username + " said:", reason)
        .setFooter("Congrats ;)");

    botstorage[member.id] = botstorage[member.id] || {};
    botstorage[member.id].verified = true;
    fs.writeFileSync(userInfoFilePath, JSON.stringify(botstorage));
    msg.channel.send(`${verifiedEmoji.toString()} Congrats! User **${member.user.username}** is now verified!`);
};
