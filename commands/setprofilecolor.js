const { cmdPrefix } = require('../bot');
const path = require('path');
const profileColorsFilePath = path.join(__dirname + '/../storage/color.json');
const botstorage = require(profileColorsFilePath); // path may vary
const fs = require('fs');

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);
    let color = args.slice(1).join(" ");

    if (!color) {
        return msg.channel.send('🎨 Please specify a color. Use hex colors only! Leave your color as `#ffffff` for the default embed color.');
    }

    if (!color.startsWith("#")) {
        color = "#" + color;
    }
    if (/^\#[A-Fa-f0-9]{3}$/.test(color)) {
        color = "#" + color[1] + color[1] + color[2] + color[2] + color[3] + color[3];
    } else if (!/^\#[A-Fa-f0-9]{6}$/.test(color)) {
        return msg.channel.send("That isn't valid, hex codes are in `#XXX` or `#XXXXXX` format, where `X` is any one of `abcdef0123456789`, case insensitive");
    }
    botstorage[msg.author.id] = color.toLowerCase();
    fs.writeFileSync(profileColorsFilePath, JSON.stringify(botstorage));

    const checkEmoji = client.emojis.cache.find(emoji => emoji.name === "tommycheck");
    msg.channel.send(checkEmoji.toString() + " Got it! Your profile color is now `" + botstorage[msg.author.id] + "`");
};
