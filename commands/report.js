const { cmdPrefix } = require('../bot');
const Discord = require('discord.js');
const path = require('path');
const reportChannelsFilePath = path.join(__dirname + '/../storage/reportchannel.json');
const botstorage = require(reportChannelsFilePath);

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);
    const userID = args[1];
    const reason = args.slice(2).join(" ");

    const member = msg.guild.members.cache.get(userID);

    if (!member) {
        const improperUsage = new Discord.MessageEmbed()
            .setTitle("How to use Report")
            .setColor("#ed411f")
            .setDescription("Use this command to report a user for any suspicious\nbehavior used with the bot.")
            .addField("Example Usage:", "$report 527523815660453889 innapropriate link")
            .setTimestamp();
        return msg.channel.send(improperUsage);
    }

    if (member.user.bot) {
        return msg.channel.send("Bro bots don't have profiles, they can't do anything wrong");
    }

    if (!reason) {
        return msg.channel.send("Look pal, if you ain't gonna give us an actual report then why submit one in the first place?");
    }

    if (!botstorage[msg.guild.id]) {
        return msg.channel.send("📣 There is no report channel set! Notify a mod immediately! This is very important.")
    }

    let rChannel = msg.guild.channels.cache.get(botstorage[msg.guild.id]);

    if (!rChannel) {
        return msg.channel.send("**🥳 You officially broke the bot!**\nYou somehow managed to submit an invalid channel ID. Or you just deleted the channel after submitting it :|");
    }

    const checkEmoji = client.emojis.cache.find(emoji => emoji.name === "tommycheck");
    const reportSuccessEmbed = new Discord.MessageEmbed()
        .setAuthor(member.user.username + " reported", member.user.displayAvatarURL({dynamic: true}))
        .setTitle("📣 Report Sent!")
        .setDescription(checkEmoji.toString() + " Your report has been sent to the reports channel")
        .setFooter("Report pending investigation")
        .setTimestamp();

    const reportEmbed = new Discord.MessageEmbed()
        .setTitle("📣 There's a report in!")
        .setColor("#fc4103")
        .setDescription("Report sent by <@" + msg.author.id + ">. They are reporting <@" + member.id + ">.")
        .addField("Reason:", reason)
        .setFooter("Please investigate this!")
        .setTimestamp();

    rChannel.send(reportEmbed);
    msg.channel.send(reportSuccessEmbed);
};
