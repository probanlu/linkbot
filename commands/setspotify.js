const { cmdPrefix } = require('../bot');
const Discord = require('discord.js');
const path = require('path');
const spotifyFilePath = path.join(__dirname + '/../storage/spotify.json');
const botstorage = require(spotifyFilePath); // path may vary
const fs = require('fs');

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);
    let link = args[1];

    if (!link) {
        const improperUsage = new Discord.MessageEmbed()
            .setTitle("How to use Set Spotify")
            .setColor("#ed411f")
            .setDescription("Use this command to set your Spotify\nthrough the bot. It will appear on your profile.")
            .addField("Example Usage:", "$setsp https://open.spotify.com/artist/4uFZsG1vXrPcvnZ4iSQyrx")
            .setTimestamp();

        return msg.channel.send(improperUsage);
    }

    if (link.toLowerCase() === "--clear") {
        msg.channel.send("Got it, your Spotify has been cleared.");
        botstorage[msg.author.id] = null;
        return fs.writeFileSync(spotifyFilePath, JSON.stringify(botstorage));
    }

    const spotifyEmoji = client.emojis.cache.find(emoji => emoji.name === "spotify");
    if (!link.startsWith('https://open.spotify.com/artist/')) {
        return msg.channel.send("Provide a Spotify link plz " + spotifyEmoji.toString());
    }

    if (link.length >= 100) {
        return msg.channel.send("Your Spotify link has to be under **100** characters!");
    }

    botstorage[msg.author.id] = "";
    fs.writeFileSync(spotifyFilePath, JSON.stringify(botstorage));

    const successEmbed = new Discord.MessageEmbed()
        .setTitle("Spotify Successfully Set!")
        .setColor("#1DB954")
        .setDescription(spotifyEmoji.toString() + " Spotify successfully set!")
        .setFooter("View your profile with $profile to see the link")
        .setTimestamp();

    msg.channel.send(successEmbed);
};
