const { cmdPrefix } = require('../bot');
const Discord = require('discord.js');
const path = require('path');
const treelinkFilePath = path.join(__dirname + '/../storage/treelink.json');
const botstorage = require(treelinkFilePath); // path may vary
const fs = require('fs');

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);
    let linktree = args[1];

    function findWord(word, str) {
        return RegExp('\\b'+ word +'\\b').test(str)
    }

    if (!linktree) {
        const improperUsage = new Discord.MessageEmbed()
            .setTitle("How to use Set Linktree")
            .setColor("#ed411f")
            .setDescription("Use this command to set your linktree\nthrough the bot. It will appear on your profile.\nLong lists of websites are for people who don't\nhave time to make a linktr.ee.")
            .addField("Example Usage:", "$setlt https://linktr.ee/guardian")
            .setTimestamp();

        return msg.channel.send(improperUsage);
    }

    if (linktree.toLowerCase() === "--clear") {
        msg.channel.send("Got it, your Linktree has been cleared.");
        botstorage[msg.author.id] = "";
        return fs.writeFileSync(treelinkFilePath, JSON.stringify(botstorage));
    }

    if (!linktree.startsWith('https://linktr.ee/')) {
        return msg.channel.send("That ain't a linktr.ee link, buddy.");
    }

    if (linktree.length >= 100) {
        return msg.channel.send("Your linktr.ee link has to be under **100** characters!");
    }

    botstorage[msg.author.id] = linktree;
    fs.writeFileSync(treelinkFilePath, JSON.stringify(botstorage));

    const checkEmoji = client.emojis.cache.find(emoji => emoji.name === "tommycheck");
    const successEmbed = new Discord.MessageEmbed()
        .setTitle("Linktree Successfully Set!")
        .setColor("#9403fc")
        .setDescription(checkEmoji.toString() + " Linktree successfully set!")
        .setFooter("View your profile with $profile")
        .setTimestamp();

    msg.channel.send(successEmbed);
};
