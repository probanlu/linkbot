const { cmdPrefix } = require('../bot');
const path = require('path');
const userInfoFilePath = path.join(__dirname + '/../storage/userinfo.json');
const botstorage = require(userInfoFilePath); // path may vary
const fs = require('fs');

module.exports = (client, msg) => {
    const args = msg.content.slice(cmdPrefix.length).trim().split(/\s+/);

    const userMention = msg.mentions.users.first();
    const id = args[1];
    let member;

    if (!msg.member.hasPermission('ADMINISTRATOR')) {
        return msg.channel.send('You don\'t have permission to verify and unverify users!');
    }

    if (!userMention) {
        member = msg.guild.members.cache.get(id);
    }

    if (userMention) {
        member = msg.guild.members.cache.get(userMention.id)
    }

    if (!userMention && !args[1]) {
        member = msg.guild.members.cache.get(msg.author.id);
    }

    if (!member) {
        return msg.channel.send('Please mention a user or paste an ID!');
    }

    botstorage[member.id] = botstorage[member.id] || {};
    botstorage[member.id].verified = false;
    fs.writeFileSync(userInfoFilePath, JSON.stringify(botstorage));
    msg.channel.send(`User **${member.user.username}** is now not verified`);
};
